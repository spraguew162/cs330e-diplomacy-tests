from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_read, diplomacy_print, diplomacy_solve

class TestDiplomacy(TestCase):
    def test_read_1(self):
        i = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        out = diplomacy_read(i)
        self.assertEqual(out,{'A': ["Madrid", []], 'B': ["Madrid", ["C"]], "C": ["London", []]})
    def test_read_2(self):
        i = ["G Austin Hold"]
        out = diplomacy_read(i)
        self.assertEqual(out, {'G': ["Austin",[]]})
    def test_read_3(self):
        i = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support A"]
        out = diplomacy_read(i)
        self.assertEqual(out,{'A': ["Madrid", ["C"]], 'B': ["Madrid", []], "C": ["London", []]})
    def test_read_4(self):
        i = ["X newYork Hold", "C Boston Move newYork", "A Austin Support C", "B Dallas Move Austin", "Z Dallas Support B"]
        out = diplomacy_read(i)
        self.assertEqual(out, {"X": ["newYork", []], "C": ["newYork", ["A"]], "A": ["Austin", []], "B": ["Austin", ["Z"]], "Z": ["Dallas", []]})
    
    def test_eval_1(self):
        i = {'A': ["Austin", []], "W": ["Temple", []], "G": ["Austin", ["W"]]}
        out = diplomacy_eval(i)
        self.assertEqual(out, {"A": "[dead]", "G": "Austin", "W": "Temple"})
    def test_eval_2(self):
        i = {"X": ["newYork", []], "C": ["newYork", ["A"]], "A": ["Austin", []], "B": ["Austin", ["Z"]], "Z": ["Dallas", []]}
        out = diplomacy_eval(i)
        self.assertEqual(out, {"X": "[dead]", "C": "[dead]", "A": "[dead]", "B": "Austin", "Z": "Dallas"})
    def test_eval_3(self):
        i = {"Y": ["Houston",[]]}
        out = diplomacy_eval(i)
        self.assertEqual(out, {"Y": "Houston"})
    
    def test_print_1(self):
        rw = StringIO()
        i = {"A": "", "B": "chicken", "C": "ChickenDinner"}
        diplomacy_print(rw,i)
        out = rw.getvalue()
        self.assertEqual(out,"A \nB chicken\nC ChickenDinner\n")
    def test_print_2(self):
        rw = StringIO()
        i = {"B": "chicken", "A": "", "C": "ChickenDinner"}
        diplomacy_print(rw,i)
        out = rw.getvalue()
        self.assertEqual(out,"A \nB chicken\nC ChickenDinner\n")
    def test_print_3(self):
        rw = StringIO()
        i = {"Gracebf": "william", "Abelus": "foreverAlone"}
        diplomacy_print(rw,i)
        out = rw.getvalue()
        self.assertEqual(out,"Abelus foreverAlone\nGracebf william\n")
    def test_print_4(self):
        rw = StringIO()
        i = {"X": "[dead]", "A": "Belarus", "B": "Bucharest", "G": "Barcelona", "D": "Budapest", "U": "[dead]"}
        diplomacy_print(rw, i)
        out = rw.getvalue()
        self.assertEqual(out, "A Belarus\nB Bucharest\nD Budapest\nG Barcelona\nU [dead]\nX [dead]\n")
    
    def test_solve_1(self):
        rw = StringIO()
        s = ["A Madrid Hold\n","B Barcelona Move Madrid\n","C London Move Madrid\n","D Paris Support B\n","E Austin Support A\n"]
        diplomacy_solve(s,rw)
        out = rw.getvalue()
        self.assertEqual(out,"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    def test_solve_2(self):
        rw = StringIO()
        s = ["A Madrid Hold\n","B Barcelona Move Madrid\n","C London Move Madrid\n"]
        diplomacy_solve(s,rw)
        out = rw.getvalue()
        self.assertEqual(out,"A [dead]\nB [dead]\nC [dead]\n")
    def test_solve_3(self):
        rw = StringIO()
        s = ["D Austin Hold\n","A Houston Support D\n","B Unova Support A\n","R Rockfort Move Houston\n","X City3 Move Unova\n"]
        diplomacy_solve(s,rw)
        out = rw.getvalue()
        self.assertEqual(out,"A [dead]\nB [dead]\nD Austin\nR [dead]\nX [dead]\n")

if __name__ == "__main__":
    main()